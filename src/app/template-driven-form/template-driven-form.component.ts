import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.css']
})
export class TemplateDrivenFormComponent implements OnInit {

  public firstname = '';
  public lastname = '';
  public gender = 'Male';
  public phoneNumber = '';
  public email = '';
  public books: any[] = [{
    title: '',
  }];


  // tslint:disable-next-line:typedef
  addBook(){
    this.books.push({
      title: '',
    });
  }
  // tslint:disable-next-line:typedef
  onSubmit(value: any) {
    console.log(value);
  }
  constructor() { }

  ngOnInit(): void {
  }

}
