import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthorValidators } from './author.validators';
@Component({
  selector: 'app-register-validation',
  templateUrl: './register-validation.component.html',
  styleUrls: ['./register-validation.component.css']
})
export class RegisterValidationComponent implements OnInit {

  form = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      gender: new FormControl('', ),
      phoneNumber: new FormControl('', Validators.required, ),
      email: new FormControl('', [Validators.required, AuthorValidators.emailValidation]),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required)
  });

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(): void{
    console.log(this.form);
  }
}
