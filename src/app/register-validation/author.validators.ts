import {AbstractControl, EmailValidator, ValidationErrors, Validators} from '@angular/forms';

export class AuthorValidators{
  static emailValidation(control: AbstractControl): { [key: string]: any } | null{
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if (control.value !== '' && (control.value.length <= 8 || !EMAIL_REGEXP.test(control.value))) {
      return {'Please provide a valid email': true};
    }
    return null;
  }
}
