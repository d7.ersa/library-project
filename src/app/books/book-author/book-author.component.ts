import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-book-author',
  templateUrl: './book-author.component.html',
  styleUrls: ['./book-author.component.css']
})
export class BookAuthorComponent implements OnInit {

  constructor() { }

  @Input('author') myAuthor: string = '';
  @Output() newAuthorEvent = new EventEmitter<string>();

  ngOnInit(): void {
  }

  addNewAuthor(value: string){
    this.newAuthorEvent.emit(value);
  }
}
