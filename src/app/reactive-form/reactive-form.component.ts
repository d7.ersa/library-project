import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  reactiveFormAuthor: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.intializeForm();
  }

  intializeForm(): void{
    this.reactiveFormAuthor = this.fb.group({
        firstname: '',
        lastname: '',
        gender: 'Male',
        phoneNumber: '',
        email: '',
        books: this.fb.array([this.fb.control('')])
    });
  }

  onSubmit(): void{
    console.log(this.reactiveFormAuthor);
  }

  addBook(): void{
    this.books.push(this.fb.control(''));
  }
  get books(): FormArray{
    return this.reactiveFormAuthor.get('books') as FormArray;
  }

}
