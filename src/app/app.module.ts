import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {LoginModule} from './login/login.module';
import {BooksModule} from './books/books.module';
import {ReactiveFormComponent} from './reactive-form/reactive-form.component';
import {TemplateDrivenFormComponent} from './template-driven-form/template-driven-form.component';
import {RegisterValidationComponent} from './register-validation/register-validation.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthenticateService} from './services/authenticate.service';
import {environment} from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegisterUserComponent } from './components/register-user/register-user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    PageNotFoundComponent,
    RegisterValidationComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    RegisterUserComponent
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    BooksModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [AuthGuardService, AuthenticateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
