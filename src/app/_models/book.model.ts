export interface Book {
  isbn: string;
  author: string;
  title: string;
  year: string;
  genre: string;
  available: boolean;
}
