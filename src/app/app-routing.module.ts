import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { BookListComponent } from './books/book-list/book-list.component';
import { CreateBookComponent } from './books/create-book/create-book.component';
import { LoginComponent } from './login/login/login.component';
import {BookComponent} from './books/book/book.component';
import {PageNotFoundComponent} from './components/page-not-found/page-not-found.component';
import {AuthGuardService} from './services/auth-guard.service';
import {RegisterUserComponent} from "./components/register-user/register-user.component";
import {BookAuthorComponent} from "./books/book-author/book-author.component";
import {BookOverviewComponent} from "./books/book-overview/book-overview.component";

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'books', component: BookListComponent},
  { path: 'createBook', component: CreateBookComponent},
  { path: 'login', component: LoginComponent },
  { path: 'book/:isbn', component: BookComponent , children: [
      { path:  'author', component: BookAuthorComponent},
      { path:  'overview', component: BookOverviewComponent},
    ]},
  { path: 'register', component: RegisterUserComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
