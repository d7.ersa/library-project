import {Component, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {Book} from '../../_models/book.model';
import {Router} from '@angular/router';
import {BooksService} from '../../services/books.service';
import {BookComponent} from '../book/book.component';



@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  // Mock data
  // async pipe will be explained after observables and rxjs
  public books: Book[] = [
    {
      isbn: '0553213695',
      author: 'Franz Kafka',
      title: 'The Metamorphosis ',
      year: 'Published March 1st 1972 by Bantam Classics',
      genre: 'Classics',
      available: true
    },
    {
      isbn: '0811201880',
      author: 'Jean-Paul Sartre',
      title: 'Nausea',
      year: 'Published 1969 by New Directions',
      genre: 'Philosophy',
      available: false
    },

  ];


  @ViewChild(BookComponent) book: BookComponent| any = null;
  @ViewChildren(BookComponent) book1: BookComponent| any = null;

  constructor(private router: Router,
              private bookService: BooksService) { }

  ngOnInit(): void {
    this.bookService.getAllBooks().subscribe( res => {
      console.log(res);
    });
  }

  ngAfterViewInit(): void{
    console.log(this.book);
    console.log(this.book1);
  }

  // tslint:disable-next-line:typedef
  onChangeAvailability(isbn: string) {
    // tslint:disable-next-line:no-shadowed-variable
    const book = this.books.find(book => book.isbn === isbn);

    if (book !== undefined) {
      book.available = !book.available;
    }
  }
  // tslint:disable-next-line:typedef
  navigateToBook(book: any) {
    const edit: boolean = book.isbn % 2 === 0 ? true : false;
    this.router.navigate(['/book', book.isbn], {queryParams: {editing: edit}});
  }
}

